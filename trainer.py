import torch
import numpy as np
import torch.nn as nn
from tqdm import tqdm



class Trainer():
    def __init__(self,train_loader,model,loss_fn,optimizer,scheduler,cuda, metrics=[]):
        super(Trainer, self).__init__()
        #self.start_epoch
        self.train_loader = train_loader
        self.model = model
        self.loss_fn = loss_fn
        self.optimizer = optimizer
        self.cuda = cuda
        #print(type(metrics))
        self.metrics = metrics
        self.scheduler = scheduler


    def train(self,n_epochs) :

        for epoch in range(n_epochs):
            self.optimizer.step()
            #self.scheduler.step()
            self.n_epochs = n_epochs
            # Train stage
            train_loss, metrics = self.train_epoch(epoch,self.train_loader, self.model, self.loss_fn, self.optimizer, self.scheduler, self.cuda, self.metrics)

            #message = 'Epoch: {}/{}. Train set: Average loss: {:.4f}'.format(epoch + 1, n_epochs, train_loss)
            #for metric in self.metrics:
            #    message += '\t{}: {}'.format(metric.name(), metric.value())
            #print(message)
            print('Saving Model.')
            torch.save(self.model,'triplet_resnet18.pth')
            print(train_loss)


    def train_epoch(self,epoch,train_loader, model, loss_fn, optimizer, scheduler, cuda, metrics):
        #print(type(metrics))
        for metric in metrics:
            metric.reset()

        model.train()
        losses = []
        total_loss = 0
        #print(train_loader)
        pbar = tqdm(total=train_loader.__len__(),desc='Epoch '+ str(epoch+1))
        for batch_idx, (data, target) in enumerate(train_loader):
            #print('Batch idx-1',batch_idx)
            pbar.update(1)
            target = target if len(target) > 0 else None
            if not type(data) in (tuple, list):
                data = (data,)
            if cuda:
                data = tuple(d.cuda() for d in data)
                if target is not None:
                    target = target.cuda()


            optimizer.zero_grad()
            outputs = model(*data)
                #print(outputs.size())
                #print(outputs)

            if type(outputs) not in (tuple, list):
                outputs = (outputs,)

            loss_inputs = outputs
            if target is not None:
                target = (target,)
                loss_inputs += target

            #print('loss input:',loss_inputs)
            loss_outputs = loss_fn(*loss_inputs)
            #print('loss output:', loss_outputs)
            loss = loss_outputs[0] if type(loss_outputs) in (tuple, list) else loss_outputs
            #print('loss :', loss.grad_fn)
            losses.append(loss.item())
            total_loss += loss.item()
            loss.backward()
            #print('loss after backward:',type(loss))
            scheduler.step()


        total_loss /= (batch_idx + 1)
        return total_loss, metrics

'''

def fit(train_loader, val_loader, model, loss_fn, optimizer, scheduler, n_epochs, cuda, log_interval, metrics=[],
        start_epoch=0):
    """
    Loaders, model, loss function and metrics should work together for a given task,
    i.e. The model should be able to process data output of loaders,
    loss function should process target output of loaders and outputs from the model

    Examples: Classification: batch loader, classification model, NLL loss, accuracy metric
    Siamese network: Siamese loader, siamese model, contrastive loss
    Online triplet learning: batch loader, embedding model, online triplet loss
    """
    for epoch in range(0, start_epoch):
        scheduler.step()

    for epoch in range(start_epoch, n_epochs):
        scheduler.step()

        # Train stage
        train_loss, metrics = train_epoch(train_loader, model, loss_fn, optimizer, cuda, log_interval, metrics)

        message = 'Epoch: {}/{}. Train set: Average loss: {:.4f}'.format(epoch + 1, n_epochs, train_loss)
        for metric in metrics:
            message += '\t{}: {}'.format(metric.name(), metric.value())

        val_loss, metrics = test_epoch(val_loader, model, loss_fn, cuda, metrics)
        val_loss /= len(val_loader)

        message += '\nEpoch: {}/{}. Validation set: Average loss: {:.4f}'.format(epoch + 1, n_epochs,
                                                                                 val_loss)
        for metric in metrics:
            message += '\t{}: {}'.format(metric.name(), metric.value())

        print(message)


def train_epoch(train_loader, model, loss_fn, optimizer, cuda, log_interval, metrics):
    for metric in metrics:
        metric.reset()

    model.train()
    losses = []
    total_loss = 0
    #print(train_loader)
    for batch_idx, (data, target) in enumerate(train_loader):
        #print('Batch idx-1',batch_idx)
        target = target if len(target) > 0 else None
        if not type(data) in (tuple, list):
            data = (data,)
        if cuda:
            data = tuple(d.cuda() for d in data)
            if target is not None:
                target = target.cuda()


        optimizer.zero_grad()
        outputs = model(*data)
        print(outputs.size())
        print(outputs)

        if type(outputs) not in (tuple, list):
            outputs = (outputs,)

        loss_inputs = outputs
        if target is not None:
            target = (target,)
            loss_inputs += target

        loss_outputs = loss_fn(*loss_inputs)
        loss = loss_outputs[0] if type(loss_outputs) in (tuple, list) else loss_outputs
        losses.append(loss.item())
        total_loss += loss.item()
        loss.backward()
        optimizer.step()

        for metric in metrics:
            metric(outputs, target, loss_outputs)

        if batch_idx % log_interval == 0:
            #print('Batch idx-2',batch_idx)
            message = 'Train: [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                batch_idx * len(data[0]), len(train_loader.dataset),
                100. * batch_idx / len(train_loader), np.mean(losses))
            for metric in metrics:
                message += '\t{}: {}'.format(metric.name(), metric.value())

            print(message)
            losses = []

    total_loss /= (batch_idx + 1)
    return total_loss, metrics


def test_epoch(val_loader, model, loss_fn, cuda, metrics):
    with torch.no_grad():
        for metric in metrics:
            metric.reset()
        model.eval()
        val_loss = 0
        for batch_idx, (data, target) in enumerate(val_loader):
            target = target if len(target) > 0 else None
            if not type(data) in (tuple, list):
                data = (data,)
            if cuda:
                data = tuple(d.cuda() for d in data)
                if target is not None:
                    target = target.cuda()

            outputs = model(*data)

            if type(outputs) not in (tuple, list):
                outputs = (outputs,)
            loss_inputs = outputs
            if target is not None:
                target = (target,)
                loss_inputs += target

            loss_outputs = loss_fn(*loss_inputs)
            loss = loss_outputs[0] if type(loss_outputs) in (tuple, list) else loss_outputs
            val_loss += loss.item()

            for metric in metrics:
                metric(outputs, target, loss_outputs)

    return val_loss, metrics
'''
