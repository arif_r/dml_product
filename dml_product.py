from torchvision import models, transforms
from cub2011 import Cub2011
import torch
import torch.optim as optim
from torch.optim import lr_scheduler
from trainer import Trainer
#import numpy as np
from losses import OnlineTripletLoss
from utils import SemihardNegativeTripletSelector
# Strategies for selecting triplets within a minibatch
from networks import EmbeddingNet#, TripletNet
#from metrics import AverageNonzeroTripletsMetric
import torch.nn as nn

class Identity(nn.Module):

    def __init__(self):
        super().__init__()

    def forward(self, x):
        return x




cuda = torch.cuda.is_available()


img_transform_train = transforms.Compose([transforms.RandomResizedCrop(size=227),
                                    transforms.RandomHorizontalFlip(0.5),
                                    transforms.ToTensor(),
                                    transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])])
img_transform_test = transforms.Compose([transforms.Resize(256),
                                    transforms.CenterCrop(227),
                                    transforms.ToTensor(),
                                    transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])])


root = '/media/arif/data/programs/dml_product_hpc_backup'
train_dataset = Cub2011(root,transform=img_transform_train,train=True,download=False)
test_dataset = Cub2011(root,transform=img_transform_test,train=False,download=False)

batch_size = 40
kwargs = {'num_workers': 4, 'pin_memory': True} if cuda else {}
train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=batch_size, shuffle=False, **kwargs)
test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=batch_size, shuffle=False, **kwargs)

# Set up the network and training parameters

#from losses import TripletLoss
#from losses import TripletLoss


margin = 1.
#embedding_net = EmbeddingNet()
#model = embedding_net
trunk = models.resnet18(pretrained=True)
trunk_output_size = trunk.fc.in_features
#print('FC:', trunk.fc.in_features)
trunk.fc = Identity()
embedder = nn.Linear(trunk_output_size, 64)

model = EmbeddingNet(trunk,embedder)

if cuda:
    model.cuda()
loss_fn =  OnlineTripletLoss(margin,SemihardNegativeTripletSelector(margin))
lr = 1e-3
optimizer = optim.Adam(model.parameters(), lr=lr, weight_decay=1e-4)
scheduler = lr_scheduler.StepLR(optimizer, 8, gamma=0.1, last_epoch=-1)
n_epochs = 1
log_interval = batch_size

#fit(train_loader, test_loader, model, loss_fn, optimizer, scheduler, n_epochs, cuda, log_interval)

trainer = Trainer(train_loader, model, loss_fn, optimizer, scheduler,cuda)
trainer.train(n_epochs)
