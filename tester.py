import torch
#import numpy as np
#import torch.nn as nn
#from tqdm import tqdm
import faiss
from tqdm import tqdm
import pickle
import numpy as np

class Tester():
    def __init__(self,test_dataset,model,cuda,metrics=[]):
        super(Tester, self).__init__()
        #self.start_epoch
        self.test_dataset = test_dataset
        self.model = model
        self.cuda = cuda


    def test(self,n_epochs) :
        self.model.eval()
        self.n_epochs = n_epochs
        print ('FC;', self.model.fc.out_features) 
        #for epoch in range(n_epochs):
        dset_len = self.test_dataset.__len__()
        dim = self.model.fc.out_features+1
        embeddings= np.ndarray((dset_len,dim))
        #embeddings= []
   
        for i in tqdm(range (dset_len)) :
            img, target = self.test_dataset.__getitem__(i)
            img = img.unsqueeze(0)
                 #print(target.size)
                 #target = torch.tensor(target).float().unsqueeze(0)
            target = torch.tensor([[target]]).float()
                 #print (img.size())
                 #print(target.size())
            with torch.no_grad():
                
                outputs = self.model(img)
                 
                 #outputs = outputs.squeeze(0)
                 #print(outputs.size())
            outputs = torch.cat((outputs,target),-1)
            embeddings[i,:] = outputs.detach().numpy()
            #embeddings= np.append(embeddings, outputs.detach().numpy())

            #embeddings.append(outputs.detach())
            #print(embeddings.shape)
                    
                 #print(outputs[:,64])
                 
                 #print(i)
                 
                 #print(i)
        
        # embeddings=[]
        # pbar = tqdm(total=self.test_loader.__len__())
        # for batch_idx, (data, target) in enumerate(self.test_loader):
        #         target = target if len(target) > 0 else None
        #         if not type(data) in (tuple, list):
        #             data = (data,)
        #         if self.cuda:
        #             data = tuple(d.cuda() for d in data)
        #             if target is not None:
        #                 target = target.cuda()
        #         #print(target.size())        
        #         outputs = self.model(*data)
        #         target = torch.unsqueeze(target.float(),-1)
                
        #         # print('output: ', outputs.size() )        
        #         # print('output: ', outputs )        
                
        #         # print('target: ', target)        
        #         # print('target: ', target )        
                
                
        #         outputs = torch.cat((outputs,target),-1)
        #         # print('output2: ', outputs.size() )        
                
        #         embeddings.append(outputs)
        #         pbar.update(1)
        #build index
        #dim = embeddings[0].size()[1]
        
        
        print('dimensions:', dim)
        index = faiss.IndexFlatL2(dim)
        index.add(embeddings.astype('float32'))
        print ('indexing done.')        
        #fhandle = open('index_restnet18.pkl','wb')
        #pickle.dump(index,fhandle)
        faiss.write_index(index,'cub2011_resnet18_index')        
                # print('sebelum: ',outputs.size())
                
                # torch.unsqueeze(outputs,-1)
                # outputs[:,-1] = target
                
                # print(outputs.size())
                
            #for batch_idx, (data, target) in enumerate(self.test_loader):
                #print(data,'->',target)
                #print(target)
            #train_loss, metrics = self.train_epoch(epoch,self.train_loader, self.model, self.loss_fn, self.optimizer, self.scheduler, self.cuda, self.metrics)

    #def get_embeddings(model,)



    
    
    def test_epoch(self,test_loader, model, cuda, metrics):
        with torch.no_grad():
            for metric in metrics:
                metric.reset()
            model.eval()
            val_loss = 0
            for batch_idx, (data, target) in enumerate(test_loader):
                target = target if len(target) > 0 else None
                if not type(data) in (tuple, list):
                    data = (data,)
                if cuda:
                    data = tuple(d.cuda() for d in data)
                    if target is not None:
                        target = target.cuda()

                outputs = model(*data)

                if type(outputs) not in (tuple, list):
                    outputs = (outputs,)
                loss_inputs = outputs
                if target is not None:
                    target = (target,)
                    loss_inputs += target

                loss_outputs = loss_fn(*loss_inputs)
                loss = loss_outputs[0] if type(loss_outputs) in (tuple, list) else loss_outputs
                val_loss += loss.item()

                for metric in metrics:
                    metric(outputs, target, loss_outputs)

        return val_loss, metrics
