import numpy as np
from tqdm import tqdm
import faiss

def get_embeddings(dataset,model) :
    model.eval()
    dset_len = dataset.__len__()
    dim = model.fc.out_features+1
    embeddings= np.ndarray((dset_len,dim),dtype='float32')
    for i in tqdm(range (dset_len)) :
        img, target = dataset.__getitem__(i)
        img = img.unsqueeze(0)
        #target = torch.tensor([[target]]).float()
        outputs = model(img)
        #print(target) #embeddings[i,:] = 
        #print(outputs.detach().numpy().shape)
        embeddings[i,:dim-1] = outputs.detach().numpy()
        embeddings[i,-1] = target
        #print(np.concatenate(outputs.detach().numpy(),target))
        #print(embeddings)
    
    np.save('embeddings_cub2011.npy',embeddings)
    return embeddings



def build_index(embeddings) :
    dim = embeddings[0].size-1
    #print(dim)
    
    #print('query: ', embeddings[1,64])
    #print('hasil: ', embeddings[9,64])
    
    index = faiss.IndexFlatL2(dim)
    #print(index.is_trained)
    #print(embeddings[:,:dim].shape)
    emb = embeddings[:,:dim]
    #print(emb.shape)
    #print(np.ascontiguousarray(emb).shape)
    index.add(np.ascontiguousarray(emb) )
    #index.add(emb)
    
    #print(emb[0,:].shape)
    #emb = np.expand_dims(emb[1,:], axis=0)
    #('query: ', embeddings[0,:])
    
    
    #D,I = index.search(emb,5)
    #print('D:', D)
    #print('I:', I)
    #print (embeddings[1461,64])
    return index
    
def get_accuracies(embeddings,k) :
    
    